import {axiosApi} from "../../axiosApi";

export const FETCH_TRACK_HISTORIES_REQUEST = 'FETCH_TRACK_HISTORIES_REQUEST';
export const FETCH_TRACK_HISTORIES_SUCCESS = 'FETCH_TRACK_HISTORIES_SUCCESS';
export const FETCH_TRACK_HISTORIES_FAILURE = 'FETCH_TRACK_HISTORIES_FAILURE';

export const CREATE_TRACK_HISTORIES_REQUEST = 'FETCH_TRACK_HISTORIES_REQUEST';
export const CREATE_TRACK_HISTORIES_SUCCESS = 'FETCH_TRACK_HISTORIES_SUCCESS';
export const CREATE_TRACK_HISTORIES_FAILURE = 'FETCH_TRACK_HISTORIES_FAILURE';

const fetchTrackHistoriesRequest = () => ({type: FETCH_TRACK_HISTORIES_REQUEST});
const fetchTrackHistoriesSuccess = tracks => ({type: FETCH_TRACK_HISTORIES_SUCCESS, payload: tracks});
const fetchTrackHistoriesFailure = e => ({type: FETCH_TRACK_HISTORIES_FAILURE, payload: e});

const createTrackHistoriesRequest = () => ({type: CREATE_TRACK_HISTORIES_REQUEST});
const createTrackHistoriesSuccess = tracks => ({type: CREATE_TRACK_HISTORIES_SUCCESS, payload: tracks});
const createTrackHistoriesFailure = e => ({type: CREATE_TRACK_HISTORIES_FAILURE, payload: e});


export const getTrackHistories = () => async (dispatch, getState) => {
    const headers = {
        "Authorization": getState().auth.authData && getState().auth.authData.token
    }
    try{
        dispatch(fetchTrackHistoriesRequest());
        const {data} = await axiosApi.get(`/trackHistories`, {headers});
        dispatch(fetchTrackHistoriesSuccess(data));
    }catch(e){
        dispatch(fetchTrackHistoriesFailure(e))
    }
};

export const createTrackHistories = (info) => async (dispatch, getState) => {
    const headers = {
        "Authorization": getState().auth.authData && getState().auth.authData.token
    }
    try{
        dispatch(createTrackHistoriesRequest());
        const {data} = await axiosApi.post(`/trackHistories`, info, {headers});
        dispatch(createTrackHistoriesSuccess(data));
    }catch(e){
        dispatch(createTrackHistoriesFailure(e))
    }
};