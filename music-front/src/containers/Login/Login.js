import React, {useEffect, useState} from 'react';
import {Avatar, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOpenOutlinedIcon from "@material-ui/icons/LockOpenOutlined";
import {Link as RouterLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {Alert} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import GoogleLogin from "../../components/UI/GoogleLogin/GoogleLogin";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";
import FormElement from "../../components/Form/FormElement";
import {clearErrorUser, loginUser} from "../../store/actions/authActions";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    alert: {
        marginTop: theme.spacing(3),
        width: '100%',
    },
}))
const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [user, setUser] = useState({
        email: '',
        password: '',

    });

    const {loginError, loginLoading} = useSelector(state => state.auth);

    useEffect(() => {
        return () => {
            dispatch(clearErrorUser());
        };
    }, [dispatch]);

    const inputChangeHandler = e => {
        const {name, value} = e.target
        setUser(pre =>
            ({...pre, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...user}));
    };

    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h6">
                    Sign In
                </Typography>
                {loginError &&
                <Alert
                    className={classes.alert}
                    severity="error"
                >{loginError.message || loginError.global}
                </Alert>
                    }
                <Grid component="form"
                      container
                      className={classes.form}
                      onSubmit={submitFormHandler}
                      spacing={2}
                >
                    <FormElement
                        required
                        type="email"
                        autoComplete="current-email"
                        label="Email"
                        onChange={inputChangeHandler}
                        name="email"
                        value={user.email}
                    />
                    <FormElement
                        required
                        type="password"
                        autoComplete="new-password"
                        label="Password"
                        onChange={inputChangeHandler}
                        name="password"
                        value={user.password}
                    />
                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            loading={loginLoading}
                            disabled={loginLoading}
                        >
                            Sign in
                        </ButtonWithProgress>
                    </Grid>
                    <Grid item xs={12}>
                        <FacebookLogin/>
                    </Grid>
                    <Grid item xs={12}>
                        <GoogleLogin/>
                    </Grid>
                    <Grid item container justifyContent="flex-end">
                        <Link component={RouterLink} variant="body2" to="/register">
                            Or sign up
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Login;