import axios from 'axios';
import {apiURL} from "./config";

export const axiosApi = axios.create({
    baseURL : apiURL,
});



