const express = require('express');
const Albums = require('../models/Album');
const path = require("path");
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});


const upload = multer({storage});

const router = express.Router();

router.get('/', async(req, res) => {
    try{
        const query = {};
        if(req.query.artist){
            query.artist = req.query.artist
        }
         const albums = await Albums.find(query).sort([['year', 1]]).populate('artist', 'name');
        res.send(albums)
    }catch{
        res.sendStatus(500);
    }
});

router.get('/:id', async(req, res) => {
    try{
        const album = await Albums.findOne({_id: req.params.id}).populate('artist', 'name description');
         if(album){
             res.send(album);
         }else{
             res.status(404).send({message: 'Album not found'});
        }
    }catch{
        res.sendStatus(500);
    }
});

router.post('/', auth, upload.single('image'), async(req, res) => {
    if(!req.body.name || !req.body.artist || !req.body.year){
        return res.status(404).send({message: 'Data not valid'});
    }

    const albumData = {
        name: req.body.name,
        artist: req.body.artist,
        year: req.body.year,
    };

    if(req.file){
        albumData.image = 'uploads/' +  req.file.filename;
    }


    const album = new Albums(albumData);
    try{
        await album.save();
        res.send(album)
    }catch(e){
        res.status(400).send({error: e});
    }
});

router.patch('/:id/publish', auth, permit('admin'), async(req, res) => {
    try{
        const album = await Albums.findById(req.params.id);
        if(!album) return res.status(400).send('Album not found');

        album.published = true;
        await Albums.findByIdAndUpdate(req.params.id, album, {new: true})
        res.send({message: 'Updated successful'})
    }catch{
        res.status(400).send({error: 'Data not valid'})
    }
});

router.delete('/:id', auth, permit('admin'), async(req, res) => {
    try{
        const album = await Albums.findById(req.params.id);
        if(!album) return res.status(400).send('Album not found');

        await Albums.findByIdAndDelete(req.params.id);
        res.send({message: 'Deleted successful'})
    }catch{

    }
})

module.exports = router;
