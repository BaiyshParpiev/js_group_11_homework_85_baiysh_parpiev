const express = require('express');
const Tracks = require('../models/Track');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Albums = require("../models/Album");

const router = express.Router();

router.get('/', async(req, res) => {
    try{
        const query = {};
        if(req.query.album){
            query.album = req.query.album
        }
        const tracks = await Tracks.find(query).sort([['datetime', -1]])
            .populate({
                path : 'album',
                populate : {
                    path : 'artist'
                }
            })

        res.send(tracks)
    }catch{
        res.sendStatus(500)
    }
});

router.post('/', async(req, res) => {
    if(!req.body.name || !req.body.album || !req.body.number){
        res.status(404).send('Data not valid');
    }

    const trackData = {
        name: req.body.name,
        album: req.body.album,
        number: req.body.number,
        duration: req.body.duration
    };

    const track = new Tracks(trackData);
    try{
        await track.save();
        res.send(track);
    }catch(e){
        res.status(400).send(e)
    }
});

router.patch('/:id/publish', auth, permit('admin'), async(req, res) => {
    try{
        const album = await Tracks.findById(req.params.id);
        if(!album) return res.status(400).send('Album not found');

        album.published = true;
        await Tracks.findByIdAndUpdate(req.params.id, album, {new: true});
        res.send({message: 'Updated successful'})
    }catch{
        res.status(400).send({error: 'Data not valid'})
    }
});
router.delete('/:id', auth, permit('admin'), async(req, res) => {
    try{
        const album = await Tracks.findById(req.params.id);
        if(!album) return res.status(400).send('Album not found');
        await Tracks.findByIdAndDelete(req.params.id);
        res.send({message: 'Deleted successful'})

    }catch{
        res.status(400).send({error: 'Data not valid'})
    }
});

module.exports = router;