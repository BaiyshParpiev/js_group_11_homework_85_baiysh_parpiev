const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  db: {
    url: 'mongodb://localhost/tracks',
  },
  google: {
    clientId: process.env.GOOGLE_LOGIN_ID,
  },
  facebook: {
    appId: process.env.FACEBOOK_ID,
    appSecret: process.env.FACEBOOK_APP_SECRET,
  },
};