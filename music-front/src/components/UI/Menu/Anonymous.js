import React from 'react';
import {Link} from "react-router-dom";
import {Button} from "@material-ui/core";

const Anonymous = () => {
    return (
       <>
           <Button component={Link} to="/login" variant="contained" color="primary">Sign In</Button>
           <Button component={Link} to="/register" variant="contained" color="primary">Sign Up</Button>
       </>
    );
};

export default Anonymous;