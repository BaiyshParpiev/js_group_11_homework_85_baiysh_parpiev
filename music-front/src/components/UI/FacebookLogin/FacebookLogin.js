import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "@material-ui/core";
import FacebookIcon from '@material-ui/icons/Facebook'
import {useDispatch} from "react-redux";
import {facebookLogin} from "../../../store/actions/authActions";
import {facebookAppId} from "../../../config";

const FacebookLogin = () => {
    const dispatch = useDispatch();

    const facebookResponse = response => {
        dispatch(facebookLogin(response));
    }
    return (
        <FacebookLoginButton
            appId={facebookAppId}
            fields="name,email,picture"
            render={props => (
                <Button
                    fullWidth
                    color="primary"
                    variant="outlined"
                    startIcon={<FacebookIcon/>}
                    onClick={props.onClick}
                >
                    Login with facebook
                </Button>
            )}
            callback={facebookResponse}
        />
    );
};

export default FacebookLogin;