const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    image: String,
    description: String,
    published: {
        type: Boolean,
        default: false,
    }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;