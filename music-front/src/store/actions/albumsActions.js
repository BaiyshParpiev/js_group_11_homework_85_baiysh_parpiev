import {axiosApi} from "../../axiosApi";
import {
    deleteError,
    deleteRequest,
    deleteSuccess,
    fetchPublishError,
    fetchPublishRequest,
    fetchPublishSuccess
} from "./adminActions";

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const GET_ALBUMS_REQUEST = 'GET_ALBUMS_REQUEST';
export const GET_ALBUMS_SUCCESS = 'GET_ALBUMS_SUCCESS';
export const GET_ALBUMS_FAILURE = 'GET_ALBUMS_FAILURE';

export const CREATE_ALBUMS_REQUEST =  'CREATE_ALBUMS_REQUEST';
export const CREATE_ALBUMS_SUCCESS =  'CREATE_ALBUMS_SUCCESS';
export const CREATE_ALBUMS_FAILURE =  'CREATE_ALBUMS_FAILURE';


const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, payload: albums});
const fetchAlbumsFailure = e => ({type: FETCH_ALBUMS_FAILURE, payload: e});

const getAlbumsRequest = () => ({type: GET_ALBUMS_REQUEST});
const getAlbumsSuccess = albums => ({type: GET_ALBUMS_SUCCESS, payload: albums});
const getAlbumsFailure = e => ({type: GET_ALBUMS_FAILURE, payload: e});

const createAlbumsRequest = () => ({type: CREATE_ALBUMS_REQUEST});
const createAlbumsSuccess = albums => ({type: CREATE_ALBUMS_SUCCESS, payload: albums});
const createAlbumsFailure = e => ({type: CREATE_ALBUMS_FAILURE, payload: e});


export const getAlbums = id => async dispatch => {
    try{
        dispatch(fetchAlbumsRequest());
        const {data} = await axiosApi.get(`/albums?artist=${id}`);
        dispatch(fetchAlbumsSuccess(data));
    }catch(e){
        dispatch(fetchAlbumsFailure(e))
    }
};

export const getAlbum = () => async dispatch => {
    try{
        dispatch(getAlbumsRequest());
        const {data} = await axiosApi.get(`/albums`);
        dispatch(getAlbumsSuccess(data));
    }catch(e){
        dispatch(getAlbumsFailure(e))
    }
};

export const fetchPublish = (id) => async dispatch => {
    try{
        dispatch(fetchPublishRequest())
        await axiosApi.patch(`/albums/${id}/publish`);
        dispatch(fetchPublishSuccess(id));
    }catch(e){
        dispatch(fetchPublishError(e))
    }
};

export const deletePublic = id => async dispatch => {
    try{
        dispatch(deleteRequest())
        await axiosApi.delete('/albums/' + id);
        dispatch(deleteSuccess(id));
    }catch(e){
        dispatch(deleteError(e))
    }
};

export const createAlbums = d => async dispatch => {
    try{
        dispatch(createAlbumsRequest());
        const {data} = await axiosApi.post(`/albums`, d);
        dispatch(createAlbumsSuccess(data));
    }catch(e){
        dispatch(createAlbumsFailure(e))
    }
};