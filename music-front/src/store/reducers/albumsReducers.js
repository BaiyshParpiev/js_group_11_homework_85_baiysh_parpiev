import {
    CREATE_ALBUMS_FAILURE,
    CREATE_ALBUMS_REQUEST, CREATE_ALBUMS_SUCCESS,
    FETCH_ALBUMS_FAILURE,
    FETCH_ALBUMS_REQUEST,
    FETCH_ALBUMS_SUCCESS, GET_ALBUMS_FAILURE, GET_ALBUMS_REQUEST, GET_ALBUMS_SUCCESS
} from "../actions/albumsActions";
import {
    DELETE_FAILURE,
    DELETE_REQUEST, DELETE_SUCCESS,
    FETCH_PUBLISH_FAILURE,
    FETCH_PUBLISH_REQUEST,
    FETCH_PUBLISH_SUCCESS
} from "../actions/adminActions";

const initialState = {
    albums: [],
    fetchLoading: null,
    error: null,
};

const albumsReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case FETCH_ALBUMS_SUCCESS:
            return {...state, fetchLoading: false, albums: action.payload};
        case FETCH_ALBUMS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case GET_ALBUMS_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case GET_ALBUMS_SUCCESS:
            return {...state, fetchLoading: false, albums: action.payload};
        case GET_ALBUMS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case CREATE_ALBUMS_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case CREATE_ALBUMS_SUCCESS:
            return {...state, fetchLoading: false, albums: [...state.albums, action.payload]};
        case CREATE_ALBUMS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case DELETE_REQUEST:
            return {...state};
        case DELETE_SUCCESS:
            return {...state, albums: state.albums.filter(a => a._id !== action.payload)};
        case DELETE_FAILURE:
            return {...state, error: action.payload};
        case FETCH_PUBLISH_REQUEST:
            return state;
        case FETCH_PUBLISH_FAILURE:
            return {...state, error: action.payload};
        case FETCH_PUBLISH_SUCCESS:
            console.log(action.payload, state.albums)
            return {...state, albums: state.albums.map(t => t._id === action.payload ? t.published = true : t), error: null};
        default:
            return state;
    }
};

export default albumsReducers;
