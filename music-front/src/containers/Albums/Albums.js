import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {getAlbums} from "../../store/actions/albumsActions";
import AlbumsCard from "../../components/AlbumsCard/AlbumsCard";
import {useLocation} from "react-router-dom";

const Albums = () => {
    const dispatch = useDispatch();
    const {albums, fetchLoading} = useSelector(state => state.albums);

    const useQuery = () => {
        const { search } = useLocation();

        return React.useMemo(() => new URLSearchParams(search), [search]);
    }

    const query = useQuery();
    const id = query.get("artist");

    useEffect(() => {
        dispatch(getAlbums(id));
    }, [dispatch, id]);

    return (
        <Grid container direction='column' spacing={2}>
            <Grid item container justifyContent='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h4'>Albums</Typography>
                </Grid>
                <Grid item>
                    <Typography variant='body1'>{!fetchLoading && albums[0]?.artist?.name}</Typography>
                </Grid>
            </Grid>
            <Grid item container direction="row" spacing={1}>
                {fetchLoading ? (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : albums.map(album => (
                    <AlbumsCard
                        key={album._id}
                        id={album._id}
                        name={album.name}
                        image={album.image}
                        year={album.year}
                        count={album.count}
                        published={album.published}
                    />
                ))}
            </Grid>
        </Grid>
    );
};
export default Albums;