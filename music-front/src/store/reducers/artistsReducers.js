import {
    CREATE_ARTISTS_FAILURE,
    CREATE_ARTISTS_REQUEST,
    CREATE_ARTISTS_SUCCESS,
    FETCH_ARTISTS_FAILURE,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS
} from "../actions/artistsActions";
import {
    DELETE_FAILURE,
    DELETE_REQUEST, DELETE_SUCCESS,
    FETCH_PUBLISH_FAILURE,
    FETCH_PUBLISH_REQUEST,
    FETCH_PUBLISH_SUCCESS
} from "../actions/adminActions";

const initialState = {
    artists: [],
    fetchLoading: null,
    error: null,
};

const artistsReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PUBLISH_REQUEST:
            return state;
        case FETCH_PUBLISH_SUCCESS:
            return {...state, artists: state.artists.map(t => t._id === action.payload ? t.published = true : t), error: null};
        case FETCH_PUBLISH_FAILURE:
            return {...state, error: action.payload};
        case DELETE_REQUEST:
            return {...state};
        case DELETE_SUCCESS:
            return {...state, artists: state.artists.filter(a => a._id !== action.payload)};
        case DELETE_FAILURE:
            return {...state, error: action.payload};
        case FETCH_ARTISTS_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case FETCH_ARTISTS_SUCCESS:
            return {...state, fetchLoading: false, artists: action.payload};
        case FETCH_ARTISTS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case CREATE_ARTISTS_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case CREATE_ARTISTS_SUCCESS:
            return {...state, fetchLoading: false, artists: [...state.artists, action.payload]};
        case CREATE_ARTISTS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default artistsReducers;
