const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const AlbumSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true,
    },
    year: {
        type: Number,
        required: true,
    },
    image: String,
    published: {
        type: Boolean,
        default: false,
    }
});

AlbumSchema.plugin(idValidator);
const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;
