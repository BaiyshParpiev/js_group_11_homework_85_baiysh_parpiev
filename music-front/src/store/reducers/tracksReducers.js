import {
    CREATE_TRACKS_FAILURE,
    CREATE_TRACKS_REQUEST, CREATE_TRACKS_SUCCESS,
    FETCH_TRACKS_FAILURE,
    FETCH_TRACKS_REQUEST,
    FETCH_TRACKS_SUCCESS
} from "../actions/tracksActions";
import {
    DELETE_FAILURE,
    DELETE_REQUEST, DELETE_SUCCESS,
    FETCH_PUBLISH_FAILURE,
    FETCH_PUBLISH_REQUEST,
    FETCH_PUBLISH_SUCCESS
} from "../actions/adminActions";

const initialState = {
    tracks: [],
    fetchLoading: null,
    error: null,
};

const tracksReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PUBLISH_REQUEST:
            return state;
        case FETCH_PUBLISH_SUCCESS:
            return {...state, tracks: state.tracks.map(t => t._id === action.payload ? t.published = true : t), error: null};
        case FETCH_PUBLISH_FAILURE:
            return {...state, error: action.payload};
        case DELETE_REQUEST:
            return {...state};
        case DELETE_SUCCESS:
            return {...state, tracks: state.tracks.filter(a => a._id !== action.payload)};
        case DELETE_FAILURE:
            return {...state, error: action.payload};
        case FETCH_TRACKS_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case FETCH_TRACKS_SUCCESS:
            return {...state, fetchLoading: false, tracks: action.payload};
        case FETCH_TRACKS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case CREATE_TRACKS_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case CREATE_TRACKS_SUCCESS:
            return {...state, fetchLoading: false, tracks: [...state.tracks, action.payload]};
        case CREATE_TRACKS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default tracksReducers;
