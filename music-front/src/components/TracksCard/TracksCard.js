import React from 'react';
import {Button, Card, CardHeader, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {deletePublic, fetchPublish} from "../../store/actions/tracksActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});


const TracksCard = ({number, name, duration, id, createHistory, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.authData);

    return user?.role === 'admin' ? (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card classame={classes.card}>
                <CardHeader title={name}/>
                <Grid item>
                    <Typography variant='h4'>Number of track: {number}</Typography>
                </Grid>
                <Grid item>
                    <Typography variant='body1'>Duration {duration}</Typography>
                </Grid>
                <Button onClick={() => createHistory(id)}>Add your History</Button>
                <Grid container justifyContent="space-between">
                    {published ?
                        <Button color="primary" onClick={() => dispatch(deletePublic(id))}>Delete</Button>
                        :
                        <Button color="primary" onClick={() => dispatch(fetchPublish(id))}>Publish</Button>
                    }
                </Grid>
            </Card>
        </Grid>
    ) : (

        <Grid container justifyContent="space-between">
            {published &&
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <Card classame={classes.card}>
                        <CardHeader title={name}/>
                        <Grid item>
                            <Typography variant='h4'>Number of track: {number}</Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>Duration {duration}</Typography>
                        </Grid>
                        <Button onClick={() => createHistory(id)}>Add your History</Button>
                    </Card>
                </Grid>
            }
        </Grid>
    )
};

export default TracksCard;