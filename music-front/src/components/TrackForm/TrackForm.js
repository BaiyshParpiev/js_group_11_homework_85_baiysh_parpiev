import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import FormElement from "../Form/FormElement";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const TrackForm = ({onSubmit, categories}) => {
    const classes = useStyles();

    const [state, setState] = useState({
        name: "",
        album: "",
        number: "",
        duration: "",
    });

    const submitFormHandler = e => {
        e.preventDefault();
        onSubmit(state);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };


    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <FormElement
                select
                options={categories}
                label="Album"
                value={state.album}
                name="album"
                onChange={inputChangeHandler}
            />
            <FormElement
                label="Name of Album"
                name="name"
                value={state.name}
                onChange={inputChangeHandler}
            />
            <FormElement
                label="Duration"
                name="duration"
                value={state.duration}
                onChange={inputChangeHandler}
            />
            <FormElement
                type="number"
                label="number"
                name="number"
                value={state.number}
                onChange={inputChangeHandler}
            />
            <Grid item xs>
                <Button type="submit" color="primary" variant="contained">Create</Button>
            </Grid>
        </Grid>
    );
};

export default TrackForm;