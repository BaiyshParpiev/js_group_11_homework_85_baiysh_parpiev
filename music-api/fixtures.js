const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Artist = require("./models/Artist");
const Album = require("./models/Album");
const Track = require("./models/Track");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [firstSinger, secondSinger, thirdSinger] = await Artist.create({
    name: 'Mirbek Atabekov',
    image: 'fixtures/mirbek.jpeg',
    description: 'Central Asia singer',
    published: true,
  }, {
    name: 'Scriptonit',
    image: 'fixtures/scriptonit.jpg',
    description: 'Russian&Kazakstan singer',
    published: true,
  }, {
    name: 'Eminem',
    image: 'fixtures/eminem.jpeg',
    description: 'Wortl rap star',
    published: true,
  });

  const [firstSingerAlbumFirst, firstSingerAlbumSecond , secondSingerAlbumFirst, secondSingerAlbumSecond, thirdSingerAlbumFirst, thirdSingerAlbumSecond] = await Album.create({
    name: 'Surdotpochu',
    artist: firstSinger,
    year: 2012,
    image: 'fixtures/firstSingerFirstAlbum.jpeg',
    published: true,
  }, {
    name: 'Kara Koz',
    artist: firstSinger,
    year: 2014,
    image: 'fixtures/firstSingerSecondAlbum.jpeg',
     published: true,
  }, {
    name: 'ACC',
    artist: secondSinger,
    year: 2018,
    image: 'fixtures/secondSingerFirstAlbum.jpeg',
     published: true,
  }, {
    name: 'VBR',
    artist: secondSinger,
    year: 2016,
    image: 'fixtures/secondSingerSecondAlbum.jpg',
     published: true,
  }, {
    name: 'Alone',
    artist: thirdSinger,
    year: 2002,
    image: 'fixtures/thirdSingerFirstAlbum.jpeg',
     published: true,
  }, {
    name: 'Intel core i7',
    artist: thirdSinger,
    year: 2005,
    image: 'fixtures/thirdSingerSecondAlbum.jpeg',
    published: true,
  });

  await Track.create({
    name: 'Surdotpochu',
    album: firstSingerAlbumFirst,
    duration: '4min',
    number: 1,
    published: true,
  }, {
    name: 'Kettin Alyska',
    album: firstSingerAlbumFirst,
    duration: '4min 20sec',
    number: 2,
    published: true,
  }, {
    name: 'Bul Duyno',
    album: firstSingerAlbumFirst,
    duration: '3min',
    number: 3,
    published: true,
  },{
    name: 'Muras',
    album: firstSingerAlbumFirst,
    duration: '4min',
    number: 4,
    published: true,
  },{
    name: 'Eto lyubov',
    album: firstSingerAlbumFirst,
    duration: '3min',
    number: 5,
    published: true,
  }, {
    name: 'Kyzyl alma',
    album: firstSingerAlbumFirst,
    duration: '4min',
    number: 6,
    published: true,
  }, {
    name: 'Kulku',
    album: firstSingerAlbumFirst,
    duration: '4min 20sec',
    number: 7,

    published: true,
  }, {
    name: 'Seni suyom',
    album: firstSingerAlbumFirst,
    duration: '3min',
    number: 8,
    published: true,
  },{
    name: 'Ata-Mekenim',
    album: firstSingerAlbumFirst,
    duration: '4min',
    number: 9,
    published: true,
  },{
    name: 'Janm',
    album: firstSingerAlbumFirst,
    duration: '3min',
    number: 10,
    published: true,
  });

  await Track.create({
    name: 'Armanym',
    album: firstSingerAlbumSecond,
    duration: '4min',
    number: 1,
    published: true,
  }, {
    name: 'Sagynam',
    album: firstSingerAlbumSecond,
    duration: '4min 20sec',
    number: 2,
    published: true,
  }, {
    name: 'Kara koz',
    album: firstSingerAlbumSecond,
    duration: '3min',
    number: 3,
    published: true,
  },{
    name: 'Muras',
    album: firstSingerAlbumSecond,
    duration: '4min',
    number: 4,
    published: true,
  },{
    name: 'Eto lyubov',
    album: firstSingerAlbumSecond,
    duration: '3min',
    number: 5,
    published: true,
  }, {
    name: 'Kyzyl alma',
    album: firstSingerAlbumSecond,
    duration: '4min',
    number: 6,
    published: true,
  }, {
    name: 'Kulku',
    album: firstSingerAlbumSecond,
    duration: '4min 20sec',
    number: 7,
    published: true,
  }, {
    name: 'Seni suyom',
    album: firstSingerAlbumSecond,
    duration: '3min',
    number: 8,
    published: true,
  },{
    name: 'Ata-Mekenim',
    album: firstSingerAlbumSecond,
    duration: '4min',
    number: 9,
    published: true,
  },{
    name: 'Janm',
    album: firstSingerAlbumSecond,
    duration: '3min',
    number: 10,
    published: true,
  });

  await Track.create({
    name: 'Kosmos',
    album: secondSingerAlbumFirst,
    duration: '4min',
    number: 1,
    published: true,
  }, {
    name: 'Mama',
    album: secondSingerAlbumFirst,
    duration: '4min 20sec',
    number: 2,
    published: true,
  }, {
    name: 'Rayon',
    album: secondSingerAlbumFirst,
    duration: '3min',
    number: 3,
    published: true,
  },{
    name: 'Kacheli',
    album: secondSingerAlbumFirst,
    duration: '4min',
    number: 4,
    published: true,
  },{
    name: 'Eto lyubov',
    album: secondSingerAlbumFirst,
    duration: '3min',
    number: 5,
    published: true,
  }, {
    name: 'Jiza',
    album: secondSingerAlbumFirst,
    duration: '4min',
    number: 6,
    published: true,
  }, {
    name: 'baby',
    album: secondSingerAlbumFirst,
    duration: '4min 20sec',
    number: 7,
    published: true,
  }, {
    name: 'Kajdiy den',
    album: secondSingerAlbumFirst,
    duration: '3min',
    number: 8,
    published: true,
  },{
    name: 'Dalshe-bolshe',
    album: secondSingerAlbumFirst,
    duration: '4min',
    number: 9,
    published: true,
  },{
    name: 'Hipe',
    album: secondSingerAlbumFirst,
    duration: '3min',
    number: 10,
    published: true,
  });

  await Track.create({
    name: 'Asuma',
    album: secondSingerAlbumSecond,
    duration: '4min',
    number: 1,
    published: true,
  }, {
    name: 'Mir',
    album: secondSingerAlbumSecond,
    duration: '4min 20sec',
    number: 2,
    published: true,
  }, {
    name: 'Ulitsa',
    album: secondSingerAlbumSecond,
    duration: '3min',
    number: 3,
    published: true,
  },{
    name: 'Jivi',
    album: secondSingerAlbumSecond,
    duration: '4min',
    number: 4,
    published: true,
  },{
    name: 'V etom mire',
    album: secondSingerAlbumSecond,
    duration: '3min',
    number: 5,
    published: true,
  }, {
    name: 'Kino',
    album: secondSingerAlbumSecond,
    duration: '4min',
    number: 6,
    published: true,
  }, {
    name: 'Tansuy',
    album: secondSingerAlbumSecond,
    duration: '4min 20sec',
    number: 7,
    published: true,
  }, {
    name: 'Strana',
    album: secondSingerAlbumSecond,
    duration: '3min',
    number: 8,
    published: true,
  },{
    name: 'dvijeniya',
    album: secondSingerAlbumSecond,
    duration: '4min',
    number: 9,
    published: true,
  },{
    name: 'Milaha',
    album: secondSingerAlbumSecond,
    duration: '3min',
    number: 10,
    published: true,
  });

  await Track.create({
    name: 'Lost',
    album: thirdSingerAlbumFirst,
    duration: '4min',
    number: 1,
    published: true,
  }, {
    name: 'Live',
    album: thirdSingerAlbumFirst,
    duration: '4min 20sec',
    number: 2,
    published: true,
  }, {
    name: 'Motivation',
    album: thirdSingerAlbumFirst,
    duration: '3min',
    number: 3,
    published: true,
  },{
    name: 'Previous',
    album: thirdSingerAlbumFirst,
    duration: '4min',
    number: 4,
    published: true,
  },{
    name: 'Nichname',
    album: thirdSingerAlbumFirst,
    duration: '3min',
    number: 5,
    published: true,
  }, {
    name: 'Action',
    album: thirdSingerAlbumFirst,
    duration: '4min',
    number: 6,
    published: true,
  }, {
    name: 'Gangster',
    album: thirdSingerAlbumFirst,
    duration: '4min 20sec',
    number: 7,
    published: true,
  }, {
    name: 'Change',
    album: thirdSingerAlbumFirst,
    duration: '3min',
    number: 8,
    published: true,
  },{
    name: 'Unbelievable',
    album: thirdSingerAlbumFirst,
    duration: '4min',
    number: 9,
    published: true,
  },{
    name: 'Baby',
    album: thirdSingerAlbumFirst,
    duration: '3min',
    number: 10,
    published: true,
  });

  await Track.create({
    name: 'The country',
    album: thirdSingerAlbumFirst,
    duration: '4min',
    number: 1,
    published: true,
  }, {
    name: 'Big city',
    album: thirdSingerAlbumFirst,
    duration: '4min 20sec',
    number: 2,
    published: true,
  }, {
    name: 'Feeling',
    album: thirdSingerAlbumFirst,
    duration: '3min',
    number: 3,
    published: true,
  },{
    name: 'Strike',
    album: thirdSingerAlbumFirst,
    duration: '4min',
    number: 4,
    published: true,
  },{
    name: 'Back to home',
    album: thirdSingerAlbumFirst,
    duration: '3min',
    number: 5,
    published: true,
  }, {
    name: 'Bring me',
    album: thirdSingerAlbumSecond,
    duration: '4min',
    number: 6,
    published: true,
  }, {
    name: 'Street language',
    album: thirdSingerAlbumSecond,
    duration: '4min 20sec',
    number: 7,
    published: true,
  }, {
    name: 'Aims',
    album: thirdSingerAlbumSecond,
    duration: '3min',
    number: 8,
    published: true,
  },{
    name: 'Space',
    album: thirdSingerAlbumSecond,
    duration: '4min',
    number: 9,
    published: true,
  },{
    name: 'Maria',
    album: thirdSingerAlbumSecond,
    duration: '3min',
    number: 10,
    published: true,
  });





  await User.create({
    displayName: "admin",
    email: 'admin@gmail.com',
    password: 'admin',
    confirmPassword: 'admin',
    role: 'admin',
    token: nanoid(),
  }, {
    displayName: "user",
    email: 'user@gmail.com',
    password: 'user',
    confirmPassword: 'user',
    token: nanoid(),
    role: 'user',
  });

  await mongoose.connection.close();
};

run().catch(console.error);