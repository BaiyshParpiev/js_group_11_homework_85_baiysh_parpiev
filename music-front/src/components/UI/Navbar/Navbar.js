import React from 'react';
import {AppBar, Grid, Toolbar, Typography} from "@material-ui/core";
import {Link} from 'react-router-dom';
import useStyles from './styles.js'
import {useDispatch, useSelector} from "react-redux";
import UserMenu from "../Menu/UserMenu";
import Anonymous from "../Menu/Anonymous";
import {logoutUser} from "../../../store/actions/authActions";

const Navbar = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);



    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Grid
                        container
                        spacing={0}
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                    >
                        <Typography component={Link} to="/" className={classes.heading}>Music-app</Typography>
                        <Grid container justifyContent="space-between" item xs={12} sm={6} md={3} className={classes.align}>
                            {user ? (
                                <>
                                    <UserMenu user={user} classes={classes} logOut={() => dispatch(logoutUser())}/>
                                    <Typography component={Link} to="/trackHistory" className={classes.heading}>Track History</Typography>
                                    <Typography component={Link} to="/addNew" className={classes.heading}>Add new</Typography>
                                </>
                            ) : (
                                <Anonymous/>
                            )}

                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        </>

    );
};

export default Navbar;