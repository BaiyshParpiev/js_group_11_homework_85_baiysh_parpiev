
export const FETCH_PUBLISH_REQUEST = 'FETCH_PUBLISH_REQUEST';
export const FETCH_PUBLISH_SUCCESS = 'FETCH_PUBLISH_SUCCESS';
export const FETCH_PUBLISH_FAILURE = 'FETCH_PUBLISH_FAILURE';


export const fetchPublishRequest = () => ({type: FETCH_PUBLISH_REQUEST});
export const fetchPublishSuccess = d => ({type: FETCH_PUBLISH_SUCCESS, payload: d});
export const fetchPublishError = e => ({type: FETCH_PUBLISH_FAILURE, payload: e});

export const DELETE_REQUEST = 'DELETE_REQUEST';
export const DELETE_SUCCESS = 'DELETE_SUCCESS';
export const DELETE_FAILURE = 'DELETE_FAILURE';

export const deleteRequest = () => ({type: DELETE_REQUEST});
export const deleteSuccess = e => ({type: DELETE_SUCCESS, payload: e});
export const deleteError = e => ({type: DELETE_FAILURE, payload: e});
