import {axiosApi} from "../../axiosApi";
import {
    deleteError,
    deleteRequest,
    deleteSuccess,
    fetchPublishError,
    fetchPublishRequest,
    fetchPublishSuccess
} from "./adminActions";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';

export const CREATE_ARTISTS_REQUEST = 'CREATE_ARTISTS_REQUEST';
export const CREATE_ARTISTS_SUCCESS = 'CREATE_ARTISTS_SUCCESS';
export const CREATE_ARTISTS_FAILURE = 'CREATE_ARTISTS_FAILURE';

const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, payload: artists});
const fetchArtistsFailure = e => ({type: FETCH_ARTISTS_FAILURE, payload: e});

const createArtistsRequest = () => ({type: CREATE_ARTISTS_REQUEST});
const createArtistsSuccess = artists => ({type: CREATE_ARTISTS_SUCCESS, payload: artists});
const createArtistsFailure = e => ({type: CREATE_ARTISTS_FAILURE, payload: e});


export const getArtists = () => async dispatch => {
    try{
        dispatch(fetchArtistsRequest());
        const {data} = await axiosApi.get('/artists');
        dispatch(fetchArtistsSuccess(data));
    }catch(e){
        dispatch(fetchArtistsFailure(e))
    }
};

export const createArtists = d => async dispatch => {
    try{
        dispatch(createArtistsRequest());
        const {data} = await axiosApi.post('/artists', d);
        dispatch(createArtistsSuccess(data));
    }catch(e){
        dispatch(createArtistsFailure(e))
    }
};

export const fetchPublish = (id) => async dispatch => {
    try{
        dispatch(fetchPublishRequest())
         await axiosApi.patch(`/artists/${id}/publish`);
        dispatch(fetchPublishSuccess(id))
    }catch(e){
        dispatch(fetchPublishError(e))
    }
};

export const deletePublic = id => async dispatch => {
    try{
        dispatch(deleteRequest())
        await axiosApi.delete('/albums/' + id);
        dispatch(deleteSuccess(id));
    }catch(e){
        dispatch(deleteError(e))
    }
};