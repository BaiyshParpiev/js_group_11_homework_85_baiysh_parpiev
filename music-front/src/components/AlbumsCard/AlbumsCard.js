import React from 'react';
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import imageNotAvailable from '../../assets/images/not_available.png';
import {Link} from "react-router-dom";
import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {deletePublic, fetchPublish} from "../../store/actions/albumsActions";


const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});


const AlbumsCard = ({name, image, id, year, count, published}) => {
    const classes = useStyles();
    let cardImage = imageNotAvailable;
    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.authData);

    if(image){
        cardImage = apiURL + '/' + image;
    }

    return user?.role === 'admin' ? (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card classame={classes.card}>
                <CardHeader title={name}/>
                <CardMedia
                    image={cardImage}
                    name={name}
                    className={classes.media}
                />
                <CardContent>
                    <Typography varian='subtitle2'>
                        {count} Musics
                    </Typography>
                    <Typography varian='subtitle2'>
                        {year}
                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/tracks?album=' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                </CardActions>
                {user?.role === 'admin' && (
                    <Grid container justifyContent="space-between">
                        {published ?
                            <Button color="primary" onClick={() => dispatch(deletePublic(id))}>Delete</Button>
                            :
                            <Button color="primary" onClick={() => dispatch(fetchPublish(id))}>Publish</Button>
                        }
                    </Grid>
                )}
            </Card>
        </Grid>
    ): (
        published &&  <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card classame={classes.card}>
                <CardHeader title={name}/>
                <CardMedia
                    image={cardImage}
                    name={name}
                    className={classes.media}
                />
                <CardContent>
                    <Typography varian='subtitle2'>
                        {count} Musics
                    </Typography>
                    <Typography varian='subtitle2'>
                        {year}
                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/tracks?album=' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    )
};

export default AlbumsCard;