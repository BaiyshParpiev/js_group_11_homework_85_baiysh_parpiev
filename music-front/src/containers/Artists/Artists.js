import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import ArtistsCard from '../../components/ArtistsCard/ArtistsCard';
import {getArtists} from "../../store/actions/artistsActions";

const Artists = () => {
    const dispatch = useDispatch();
    const {artists, fetchLoading} = useSelector(state => state.artists);


    useEffect(() => {
        dispatch(getArtists());
    }, [dispatch])
    return (
        <Grid container direction='column' spacing={2}>
            <Grid item container justifyContent='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h4'>Artists</Typography>
                </Grid>
            </Grid>
            <Grid item container direction="row" spacing={1}>
                {fetchLoading ? (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : artists.map(artist => (
                    <ArtistsCard
                        key={artist._id}
                        id={artist._id}
                        name={artist.name}
                        image={artist.image}
                        published={artist.published}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Artists;