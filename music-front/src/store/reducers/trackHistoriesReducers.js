import {FETCH_TRACK_HISTORIES_FAILURE, FETCH_TRACK_HISTORIES_REQUEST, FETCH_TRACK_HISTORIES_SUCCESS} from "../actions/trackHistoriesActions";

const initialState = {
    tracks: [],
    fetchLoading: null,
    error: null,
};

const trackHistoriesReducers = (state = initialState, action) => {
    switch (action.type){
        case FETCH_TRACK_HISTORIES_REQUEST:
            return {...state, fetchLoading: true, error: null};
        case FETCH_TRACK_HISTORIES_SUCCESS:
            return {...state, fetchLoading: false, tracks: action.payload};
        case FETCH_TRACK_HISTORIES_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default trackHistoriesReducers;
