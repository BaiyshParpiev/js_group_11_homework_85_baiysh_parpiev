import React from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {createArtists} from "../../store/actions/artistsActions";
import ArtistForm from "../../components/ArtistForm/ArtistForm";

const NewArtist = ({history}) => {
    const dispatch = useDispatch();

    const onSubmit = async data => {
        await dispatch(createArtists(data));
        history.replace('/');
    };

    return (
        <>
            <Typography variant="h4">New Artist</Typography>
            <ArtistForm
                onSubmit={onSubmit}
            />
        </>
    );
};

export default NewArtist;