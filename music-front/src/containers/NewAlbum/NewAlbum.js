import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import AlbumsForm from "../../components/AlbumsForm/AlbumsForm";
import {useDispatch, useSelector} from "react-redux";
import {getArtists} from "../../store/actions/artistsActions";
import {createAlbums} from "../../store/actions/albumsActions";

const NewAlbum = ({history}) => {
  const dispatch = useDispatch();
  const {artists} = useSelector(state => state.artists);

  useEffect(() => {
    dispatch(getArtists());
  }, [dispatch]);

  const onSubmit = async data => {
    await dispatch(createAlbums(data));
    history.replace('/');
  };

  return (
    <>
      <Typography variant="h4">New Album</Typography>
      <AlbumsForm
        onSubmit={onSubmit}
        categories={artists}
      />
    </>
  );
};

export default NewAlbum;