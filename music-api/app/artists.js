const express = require('express');
const Artists = require('../models/Artist');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const auth  = require('../middleware/auth')
const permit = require("../middleware/permit");
const Albums = require("../models/Album");


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


const router = express.Router();

router.get('/', async(req, res) => {
    try{
        const artists = await Artists.find();
        res.send(artists)
    }catch{
        res.sendStatus(500);
    }
});

router.post('/', auth, upload.single('image'), async(req, res) => {
    if(!req.body.name){
        return res.status(404).send('Data not valid');
    }

    const artistData = {
        name: req.body.name,
        description: req.body.description || null
    };

    if(req.file){
        artistData.image = 'uploads/' + req.file.filename;
    }

    const artist = new Artists(artistData)
    try{
        await artist.save();
        res.send(artist)
    }catch(e){
        res.status(400).send(e);
    }
});

router.patch('/:id/publish', auth, permit('admin'), async(req, res) => {
    try{
        const album = await Artists.findById(req.params.id);
        if(!album) return res.status(400).send('Artist not found');

        album.published = true;
        await Artists.findByIdAndUpdate(req.params.id, album, {new: true});
        res.send({message: 'Updated successful'})
    }catch{
        res.status(400).send({error: 'Data not valid'})
    }
});

router.delete('/:id', auth, permit('admin'), async(req, res) => {
    try{
        const album = await Artists.findById(req.params.id);
        if(!album) return res.status(400).send('Artist not found');

        await Artists.findByIdAndDelete(req.params.id);
        res.send({message: 'Deleted successful'})
    }catch{
        res.status(400).send({error: 'Data not valid'})
    }
})

module.exports = router;
