import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getTrackHistories} from "../../store/actions/trackHistoriesActions";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import HistoryCard from "../../components/HistoryCard/HistoryCard";

const TrackHistory = () => {
    const dispatch = useDispatch();
    const {tracks, fetchLoading} = useSelector(state => state.trackHistories)

    useEffect(() => {
        dispatch(getTrackHistories());
    }, [dispatch]);




    return (
        <Grid container direction='column' spacing={2}>
            <Grid item container justifyContent='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h4'>Tracks History</Typography>
                </Grid>
            </Grid>
            <Grid item container direction="row" spacing={1}>
                {fetchLoading ? (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : (tracks.length > 0 ?
                    tracks.map(track => (
                            <HistoryCard
                                key={track._id}
                                id={track._id}
                                name={track.track.name}
                                artist={track.track.album.artist.name}
                                datetime={track.datetime}
                            />
                        )
                    ) : <div>You don't have any tracks</div>)}
            </Grid>
        </Grid>
    );
};

export default TrackHistory;