import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getAlbum} from "../../store/actions/albumsActions";
import {createTracks} from "../../store/actions/tracksActions";
import TrackForm from "../../components/TrackForm/TrackForm";

const NewTrack = ({history}) => {
    const dispatch = useDispatch();
    const {albums} = useSelector(state => state.albums);

    useEffect(() => {
        dispatch(getAlbum());
    }, [dispatch]);

    const onSubmit = async data => {
        await dispatch(createTracks(data));
        history.replace('/');
    };

    return (
        <>
            <Typography variant="h4">New Track</Typography>
            <TrackForm
                onSubmit={onSubmit}
                categories={albums}
            />
        </>
    );
};

export default NewTrack;