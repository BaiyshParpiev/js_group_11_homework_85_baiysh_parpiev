import React from 'react';
import {Card, CardHeader, Grid, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});


const TracksCard = ({datetime, name, artist}) => {
    const classes = useStyles();

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card classame={classes.card}>
                <CardHeader title={name}/>
                <Grid item>
                    <Typography variant='h4'>Name: {name}</Typography>
                </Grid>
                <Grid item>
                    <Typography variant='h4'>Artist: {artist}</Typography>
                </Grid>
                <Grid item>
                    <Typography variant='body1'>Date time:  {datetime}</Typography>
                </Grid>
            </Card>
        </Grid>
    );
};

export default TracksCard;