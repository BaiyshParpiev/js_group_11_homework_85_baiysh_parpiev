import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import {useSelector} from "react-redux";
import AddNew from "./containers/Add New/AddNew";
import NewTrack from "./containers/NewTrack/NewTrack";
import NewAlbum from "./containers/NewAlbum/NewAlbum";
import NewArtist from "./containers/NewArtist/NewArtist";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";

const App = () => {
    const {user} = useSelector(state => state.auth);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> : <Redirect to={redirectTo}/>
    };
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists}/>
                <Route path="/albums" component={Albums}/>
                <Route path="/tracks" component={Tracks}/>
                <Route path="/login" component={Login}/>
                <Route path="/register" component={Register}/>
                <ProtectedRoute
                    path="/trackHistory"
                    component={TrackHistory}
                    isAllowed={user?.role === 'user' || 'admin'}
                    redirectTo="/register"
                />
                <ProtectedRoute
                    path="/newTrack"
                    component={NewTrack}
                    isAllowed={user?.role === 'user' || 'admin'}
                    redirectTo="/register"
                />
                <ProtectedRoute
                    path="/newAlbum"
                    component={NewAlbum}
                    isAllowed={user?.role === 'user' || 'admin'}
                    redirectTo="/register"
                />
                <ProtectedRoute
                    path="/newArtist"
                    component={NewArtist}
                    isAllowed={user?.role === 'user' || 'admin'}
                    redirectTo="/register"
                />
                <ProtectedRoute
                    path="/addNew"
                    component={AddNew}
                    isAllowed={user?.role === 'user' || 'admin'}
                    redirectTo="/register"
                />
            </Switch>
        </Layout>
    );
}

export default App;
