import React from 'react';
import {AppBar, makeStyles, Toolbar} from "@material-ui/core";
import Navbar from "../Navbar/Navbar";

const useStyles = makeStyles(theme => ({
    mainLink:{
        color: 'inherit',
        textDecoration: 'none',
        '&:hover': {
            color: 'inherit'
        }
    },
    staticToolbar : {
        marginBottom: theme.spacing(4)
    }
}));

const AppToolbar = () => {
    const classes = useStyles();
    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                        <Navbar/>
                </Toolbar>
            </AppBar>
            <Toolbar className={classes.staticToolbar}/>
        </>
    );
};

export default AppToolbar;