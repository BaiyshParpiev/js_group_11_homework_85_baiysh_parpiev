import React from 'react';
import {Button, Grid} from "@material-ui/core";
import {Link} from "react-router-dom";

const AddNew = () => {
    return (
        <Grid container flexdirection="column">
            <Button component={Link} to="/newAlbum" fullWidth variant="contained" color="primary">Add new Album</Button>
            <Button component={Link} to="/newTrack" fullWidth variant="contained" color="primary">Add new Track</Button>
            <Button component={Link} to="/newArtist" fullWidth variant="contained" color="primary">Add new Artist</Button>
        </Grid>
    );
};

export default AddNew;