import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {getTracks} from "../../store/actions/tracksActions";
import TracksCard from "../../components/TracksCard/TracksCard";
import {useLocation} from "react-router-dom";
import {createTrackHistories} from "../../store/actions/trackHistoriesActions";

const Tracks = () => {
    const dispatch = useDispatch();
    const {tracks, fetchLoading} = useSelector(state => state.tracks);


    const useQuery = () => {
        const { search } = useLocation();

        return React.useMemo(() => new URLSearchParams(search), [search]);
    }

    const query = useQuery();
    const id = query.get("album");

    useEffect(() => {
        dispatch(getTracks(id));
    }, [dispatch, id]);

    const createHistory = id => {
        dispatch(createTrackHistories({track: id}));
    };




    return (
        <Grid container direction='column' spacing={2}>
            <Grid item container justifyContent='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h4'>Tracks</Typography>
                </Grid>
                <Grid item>
                    <Typography variant='body1'>{!fetchLoading && tracks[0]?.album?.name} {!fetchLoading && tracks[0]?.album?.artist.name}</Typography>
                </Grid>
            </Grid>
            <Grid item container direction="row" spacing={1}>
                {fetchLoading ? (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : tracks.map(track => (
                    <TracksCard
                        key={track._id}
                        id={track._id}
                        name={track.name}
                        number={track.number}
                        duration={track.duration}
                        published={track.published}
                        createHistory={createHistory}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Tracks;