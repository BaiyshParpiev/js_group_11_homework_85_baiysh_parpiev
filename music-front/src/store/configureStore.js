import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import artistsReducers from "./reducers/artistsReducers";
import albumsReducers from "./reducers/albumsReducers";
import tracksReducers from "./reducers/tracksReducers";
import trackHistoriesReducers from "./reducers/trackHistoriesReducers";
import {axiosApi} from "../axiosApi";
import usersReducer, {initialState} from "./reducers/authReducer";

const rootReducer = combineReducers({
  'artists': artistsReducers,
  'albums': albumsReducers,
  'tracks': tracksReducers,
  'auth': usersReducer,
  'trackHistories': trackHistoriesReducers,
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk))
);

store.subscribe(() => {
  saveToLocalStorage({
    auth: {
      ...initialState,
      user: store.getState().auth.user
    },
  });
});

axiosApi.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = store.getState().auth.user.token
  } catch (e) {}

  return config;
});

axiosApi.interceptors.response.use(res => res, e => {
  if (!e.response) {
    e.response = {data: {global: 'No internet'}};
  }

  throw e;
});

export default store;