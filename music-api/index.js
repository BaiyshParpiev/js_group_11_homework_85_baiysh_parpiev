require('dotenv').config();
const express = require('express');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const config = require('./config');
const album = require('./app/albums');
const artist = require('./app/artists');
const track = require('./app/tracks');
const mongoose = require('mongoose');
const users = require('./app/users');
const trackHistories = require('./app/trackHistories');

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));
const port = 8000;

app.use('/albums', album);
app.use('/artists', artist);
app.use('/tracks', track);
app.use('/users', users);
app.use('/trackHistories', trackHistories)


const run = async() => {
    await mongoose.connect(config.db.url)
    app.listen(port, () => {
        console.log('Server start on ', port, '!')
    });

    exitHook(() => {
        console.log('End of DB');
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e))

