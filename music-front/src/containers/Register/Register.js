import React, {useEffect, useState} from 'react';
import {Link as RouterLink} from 'react-router-dom'
import {Avatar, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {useDispatch, useSelector} from "react-redux";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FormElement from "../../components/Form/FormElement";
import {clearErrorUser, registerUser} from "../../store/actions/authActions";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";
import GoogleLogin from "../../components/UI/GoogleLogin/GoogleLogin";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}))

const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {registerError, registerLoading} = useSelector(state => state.auth)

    const [user, setUser] = useState({
        email: '',
        displayName: '',
        password: '',
        avatar: '',
    });

    useEffect(() => {
        return () => {
            dispatch(clearErrorUser());
        };
    }, [dispatch]);

    const inputChangeHandler = e => {
        const {name, value} = e.target
        setUser(pre =>
            ({...pre, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...user}));
    };

    const getFieldError = fieldName => {
        try{
            return registerError.errors[fieldName].message;
        }catch(e){
            return undefined;
        }
    }

    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h6">
                    Sign Up
                </Typography>
                <Grid component="form"
                      container
                      className={classes.form}
                      onSubmit={submitFormHandler}
                      spacing={2}
                      noValidate
                >
                    <FormElement
                        required
                        type="email"
                        autoComplete="new-email"
                        label="Email "
                        onChange={inputChangeHandler}
                        name="email"
                        value={user.email}
                        error={getFieldError('email')}
                    />
                    <FormElement
                        required
                        type="text"
                        autoComplete="new-displayName"
                        label="Display name"
                        onChange={inputChangeHandler}
                        name="displayName"
                        value={user.displayName}
                        error={getFieldError('displayName')}
                    />
                    <FormElement
                        required
                        type="password"
                        autoComplete="new-password"
                        label="Password"
                        onChange={inputChangeHandler}
                        name="password"
                        value={user.password}
                        error={getFieldError('password')}
                    />
                    <FormElement
                        required
                        type="text"
                        autoComplete="new-url"
                        label="Avatar link"
                        onChange={inputChangeHandler}
                        name="avatar"
                        value={user.avatar}
                        error={getFieldError('avatar')}
                    />
                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            loading={registerLoading}
                            disabled={registerLoading}
                        >
                            Sign up
                        </ButtonWithProgress>
                    </Grid>
                    <Grid item xs={12}>
                        <FacebookLogin/>
                    </Grid>
                    <Grid item xs={12}>
                        <GoogleLogin/>
                    </Grid>
                    <Grid item container justifyContent="flex-end">
                        <Link component={RouterLink} variant="body2" to="/login">
                            Already have an account? Sign In
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Register;