import React, {useState} from 'react';
import {Avatar, Button, Menu, MenuItem, Typography} from "@material-ui/core";

const UserMenu = ({user, classes, logOut}) => {
    const [anchorEl, setAnchorEl] = useState(null);
    console.log(user)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
                <div className={classes.profile}>
                    <Avatar className={classes.purple}
                            alt={user?.displayName}
                            src={user?.avatar}
                    >
                        {user?.displayName.charAt(0)}
                    </Avatar>
                    <Typography
                        className={classes.userName}
                        variant="h6"
                    >
                        {user.displayName}
                    </Typography>
                </div>
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem>My account</MenuItem>
                <MenuItem><Button
                    variant="contained"
                    color="secondary"
                    onClick={logOut}
                >
                    Logout
                </Button></MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;