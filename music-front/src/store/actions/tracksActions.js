import {axiosApi} from "../../axiosApi";
import {
    deleteError,
    deleteRequest,
    deleteSuccess,
    fetchPublishError,
    fetchPublishRequest,
    fetchPublishSuccess
} from "./adminActions";

export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';

export const CREATE_TRACKS_REQUEST = 'CREATE_TRACKS_REQUEST';
export const CREATE_TRACKS_SUCCESS = 'CREATE_TRACKS_SUCCESS';
export const CREATE_TRACKS_FAILURE = 'CREATE_TRACKS_FAILURE';

const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, payload: tracks});
const fetchTracksFailure = e => ({type: FETCH_TRACKS_FAILURE, payload: e});

const createTracksRequest = () => ({type: CREATE_TRACKS_REQUEST});
const createTracksSuccess = tracks => ({type: CREATE_TRACKS_SUCCESS, payload: tracks});
const createTracksFailure = e => ({type: CREATE_TRACKS_FAILURE, payload: e});


export const getTracks = id => async dispatch => {
    try{
        dispatch(fetchTracksRequest());
        const {data} = await axiosApi.get(`/tracks?album=${id}`);
        dispatch(fetchTracksSuccess(data));
    }catch(e){
        dispatch(fetchTracksFailure(e))
    }
};

export const createTracks = d => async dispatch => {
    try{
        dispatch(createTracksRequest());
        const {data} = await axiosApi.post(`/tracks`, d);
        dispatch(createTracksSuccess(data));
    }catch(e){
        dispatch(createTracksFailure(e))
    }
};
export const fetchPublish = (id) => async dispatch => {
    try{
        dispatch(fetchPublishRequest())
        await axiosApi.patch(`/tracks/${id}/publish`);
        dispatch(fetchPublishSuccess(id))
    }catch(e){
        dispatch(fetchPublishError(e))
    }
};

export const deletePublic = id => async dispatch => {
    try{
        dispatch(deleteRequest())
        await axiosApi.delete('/tracks/' + id);
        dispatch(deleteSuccess(id));
    }catch(e){
        dispatch(deleteError(e))
    }
};